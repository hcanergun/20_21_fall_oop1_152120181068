#include <iostream>
#include <fstream>//libraries that I used
#include <string>
using namespace std;

int Sum(int*, int);
int Product(int*, int);  ///Functions
int Smallest(int*, int);

int main() {

	int* Nums, size, Order = 0;///Variables

	fstream input_file;
	input_file.open("input.txt", ios::in | ios::out);///opening file 
	if (input_file.is_open()) {///checking if file is open or not

		input_file >> size;///taking data

		if (size<=0) {///if numbers doesn't match 
			cout << "Invalid size\n\n"; ///error message
			system("pause");///stop
		}///end-if
		Nums = new int[size];///Dynamic Memory

		///input from the file
		while (input_file) {///takeing data from file
			input_file >> *(Nums + Order);///placing data inside pointer array
			Order++;	
		}///end-while

		input_file.close();///close file

		if (Order != size + 1) {///if size doesn't match
			cout << "Unable to read from file\n\n";///error message
			system("pause");
		}///end-if
		///output(s)
		cout << "Sum is " << Sum(Nums, size) << endl;  ///printing sum
		cout << "Product is " << Product(Nums, size) << endl;  ///printing product
		cout << "Average is " << (double)Sum(Nums, size) / size << endl;  ///printing avg
		cout << "Smallest is " << Smallest(Nums, size) << endl;  ///printing smallest
	}
	else
		cout << "Unable to open file\n\n";///error message
	///end-if

	system("pause");///stop
}

///----------------------------------------------------------------
/// Adding the numbers
///
int Sum(int* Nums, int size) { ///Sum function
	int sum = 0	;

	for (int i = 0; i < size; i++)///adding numbers
		sum += *(Nums + i);
	///end-for
	return sum;///return sum
}///end-Sum


///----------------------------------------------------------------
/// multiplying numbers
///
int Product(int* Nums, int size) { ///Product function
	int product = 1;
	for (int i = 0; i < size; i++)
		product *= *(Nums + i);
	///end-for
	return product;///return product
}///end-Product


///----------------------------------------------------------------
/// find the smallest number
///
int Smallest(int* Nums, int size) { ///smallest function
	int smallest = *Nums;//first num=smallest
	for (int i = 0; i < size; i++) {
		if (*(Nums + i) < smallest)
			smallest = *(Nums + i);
		///end-if
	}///end-for
	return smallest; /// return product
}///end-Smallest