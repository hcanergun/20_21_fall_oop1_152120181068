#include <iostream>
#include <map>///new library
#include <sstream>
#include <string>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int n, q;
    string curr = "", attr_name;
    map<string, string> m;
    cin >> n >> q;
    cin.ignore();

    for (int i = 0; i < n; i++) {
        string line, tag, out;
        getline(cin, line);
        stringstream ss(line);

        while (getline(ss, out, ' ')) {
           
            if (out[0] == '<') {///if tag
               
                if (out[1] != '/') {/// opening tag
                   ///removing
                    tag = out.substr(1);
                   
                    if (tag[tag.length() - 1] == '>')
                        tag.pop_back();

                   
                    if (curr.size() > 0)  
                        curr += "." + tag;
                    else
                        curr = tag;
                }
                else {///closing tag
                    tag = out.substr(2, (out.find('>') - 2));
                    
                    size_t pos = curr.find("." + tag);
                    if (pos != string::npos)                      
                        curr = curr.substr(0, pos);
                    else ///closed
                        curr = "";
                }
            }
            else if (out[0] == '"') {///atribute value
                size_t pos = out.find_last_of('"');
                string attr_value = out.substr(1, pos - 1);
                m[attr_name] = attr_value;
            }
            else {
                if (out != "=")///atribute name
                    attr_name = curr + "~" + out;
            }
        }
    }
    ///querry 

    string query;
    for (int i = 0; i < q; i++) {
        getline(cin, query);

        /// Search in the map
        map<string, string>::iterator itr = m.find(query);
        if (itr != m.end())
            cout << itr->second << endl;
        else 
            cout << "Not Found!" << endl;
    }

   system("pause");
}