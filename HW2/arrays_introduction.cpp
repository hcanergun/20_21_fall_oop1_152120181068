#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /// Enter your code here. Read input from STDIN. Print output to STDOUT 
    int range, arr[10000], cnt = 0;
    cin >> range;///range of array
    for (int i = 0; i < range; i++)///taking data and placing inside array
        cin >> arr[i];
    ///end-for

    for (int i = range - 1; i >= 0; i--)///reverse output array
        cout << arr[i] << " ";
    ///end-for
    return 0;
}
