///#include <bits/stdc++.h>
#include <iostream>
using namespace std;
class Box
{
public:
    int l, b, h;
    ///destructions Box(),  Box(int l1, int b1, int h1),  Box(Box& B)
    Box()
    {
        l = 0;
        b = 0;
        h = 0;
    }
    Box(int l1, int b1, int h1)
    {
        l = l1;
        b = b1;
        h = h1;
    }
    Box(Box& B)
    {
        l = B.l;
        b = B.b;
        h = B.h;
    }
    ///get functions for calling variables
    int getLength() {
        return l;
    }
    int getBreadth() {
        return b;
    }
    int getHeight() {
        return h;
    }
    long long CalculateVolume() {
        return (long long)l * b * h;
    }

    bool operator<(const Box& B) {/// if one condition is true than return True
        if (l < B.l)
            return true;

        if (b < B.b && l == B.l)
            return true;

        if (h < B.h && l == B.l && b == B.b)
            return true;

        return false;
    }
};

ostream& operator<<(ostream& out, const Box& B) {///printing the variables
    out << B.l << " " << B.b << " " << B.h;
    return out;
}


void check2()
{
    int n;
    cin >> n;
    Box temp;
    for (int i = 0; i < n; i++)
    {
        int type;
        cin >> type;
        if (type == 1)
        {
            cout << temp << endl;
        }
        if (type == 2)
        {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            temp = NewBox;
            cout << temp << endl;
        }
        if (type == 3)
        {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            if (NewBox < temp)
            {
                cout << "Lesser\n";
            }
            else
            {
                cout << "Greater\n";
            }
        }
        if (type == 4)
        {
            cout << temp.CalculateVolume() << endl;
        }
        if (type == 5)
        {
            Box NewBox(temp);
            cout << NewBox << endl;
        }

    }
}

int main()
{
    check2();
}