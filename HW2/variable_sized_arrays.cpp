#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /// Enter your code here. Read input from STDIN. Print output to STDOUT 
    int arrValue, query, size;
    cin >> arrValue >> query;

    int** arr = new int* [arrValue];///1d pointer array for nums
    int** location = new int* [query];///1d pointer array for locations

    for (int i = 0; i < arrValue; i++) {
        cin >> size;
        arr[i] = new int[size];///arrenging space for upcoming arrays
        for (int j = 0; j < size; j++)
            cin >> arr[i][j];///placing arrays one by one
    }

    for (int i = 0; i < query; i++)///creating 2d locations array for number of locations
        location[i] = new int[2];

    for (int i = 0; i < query; i++) {///taking locations and printing result
        cin >> location[i][0] >> location[i][1];
        cout << arr[location[i][0]][location[i][1]] << endl;
    }




    return 0;
}