#include <iostream>
#include <string>
using namespace std;

int main() {
    /// Complete the program
    string a, b;
    char hold;
    cin >> a >> b;
    cout << a.size() << " " << b.size() << endl;///.size() size of string
    cout << a + b << endl;/// print result

    hold = a[0];/// hold for not losing a[0]
    a[0] = b[0];
    b[0] = hold;
    cout << a << " " << b;/// print result
    return 0;
}